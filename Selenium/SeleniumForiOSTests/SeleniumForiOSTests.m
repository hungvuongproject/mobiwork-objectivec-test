//
//  SeleniumForiOSTests.m
//  SeleniumForiOSTests
//
//  Created by Dan Cuellar on 28/10/2014.
//  Copyright (c) 2014 Appium. All rights reserved.
//

//#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "SERemoteWebDriver.h"
#import "SECapabilities.h"
#import "SEBy.h"
#import "SEWebElement.h"

//Working with excel
#import "BRAOfficeDocumentPackage.h"

SERemoteWebDriver *driver;
NSString *imagePath;
NSString *currentRunningMethod;
BRAOfficeDocumentPackage *spreadsheet;
NSString *fullExcelPath;

@interface SeleniumForiOSTests : XCTestCase

@end

@implementation SeleniumForiOSTests

+ (void)setUp {
    SECapabilities *caps = [SECapabilities new];
    [caps addCapabilityForKey:@"appium-version" andValue:@"1.0"];
    [caps setPlatformName:@"iOS"];
    [caps setPlatformVersion:@"8.1.2"];
    [caps setDeviceName:@"iPhone 5"];
    [caps setApp:@"/Users/vuongthanhhung/Desktop/[MobiWork][i-funbox.com].ipa"];
    NSError *error;
    driver = [[SERemoteWebDriver alloc] initWithServerAddress:@"127.0.0.1" port:4723 desiredCapabilities:caps requiredCapabilities:nil error:&error];
//    [driver setImagePath:@"/Users/vuongthanhhung/Desktop/MBW-Screenshots/"];
//    [[NSUserDefaults standardUserDefaults] setObject:@"/Users/vuongthanhhung/Desktop/MBW-Screenshots/" forKey:@"Image Path"];
    [driver setTimeout:30 forType:SELENIUM_TIMEOUT_IMPLICIT];
     imagePath = @"/Users/vuongthanhhung/Desktop/MBW-Screenshots/";
    
    fullExcelPath = @"/Users/vuongthanhhung/Desktop/MobiworkExcel/MBtestWorkbook.xlsx";
    spreadsheet  = [BRAOfficeDocumentPackage open:fullExcelPath];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
//    [driver quit];
    //Save
    [spreadsheet save];
    
    //Save a copy
//    NSString *fullPath = @"/Users/vuongthanhhung/Desktop/MobiworkExcel/MBtestWorkbook.xlsx";//[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"workbookCopy.xlsx"];
//    
//    [spreadsheet saveAs:fullPath];
}

//Override error method to do save image if error happen
- (void)recordFailureWithDescription:(NSString *)description inFile:(NSString *)filePath atLine:(NSUInteger)lineNumber expected:(BOOL)expected {
    if (description.length>0) {
        
        NSLog(@"Error Message: %@: %@", currentRunningMethod, description);
        
        UIImage *image = [driver screenshot];
        
        //Save image to local
        
        NSData *imageData = UIImagePNGRepresentation(image);
        
        //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //    NSString *documentsDirectory = [paths objectAtIndex:0];
        
//        NSString *currentRunningMethodName = NSStringFromSelector(_cmd);
        NSString *imgPath =[imagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-Line-%lu.jpg", currentRunningMethod ,(unsigned long)lineNumber]];
        
        NSLog((@"pre writing to file"));
        
        NSError *error;
        
//        if (![imageData writeToFile:imagePath atomically:NO])
        if (![imageData writeToFile:imgPath options:NSDataWritingAtomic error:&error]) {
            NSLog(@"Failed to cache image data to disk: %@", error);
        }
        else {
            NSLog(@"the cachedImagedPath is %@",imgPath);
            
            //If image successed write to path
            //Then add method, error, image name to excel file
            
            [self exportToExcelFileWithError:description atLineNumber:lineNumber imageLink:imgPath image:image];
        }
    }
}

- (void)exportToExcelFileWithError:(NSString*)errorStr atLineNumber:(NSUInteger)lineNumber imageLink:(NSString*)imageLink image:(UIImage*)img {
    
    //First worksheet in the workbook
    BRAWorksheet *worksheet = spreadsheet.workbook.worksheets[0];
    
    [[worksheet cellForCellReference:@"A1" shouldCreate:YES] setStringValue:errorStr];
    [[worksheet cellForCellReference:@"B1" shouldCreate:YES] setIntegerValue:lineNumber];
    [[worksheet cellForCellReference:@"C1" shouldCreate:YES] setStringValue:imageLink];
    
    //preserveTransparency force JPEG (NO) or PNG (YES)
    BRAWorksheetDrawing *drawing = [worksheet addImage:img betweenCellsReferenced:@"D1" and:@"F10"
                                            withInsets:UIEdgeInsetsZero preserveTransparency:NO];
    //Set drawing insets (percentage)
    drawing.insets = UIEdgeInsetsMake(0., 0., .5, .5);
}

- (void)test0_TermAndConditionScreenLayout {
    currentRunningMethod = NSStringFromSelector(_cmd);
    
    SEWebElement *acceptBtn = [driver findElementBy:[SEBy name:@"同意する"]];
    if (acceptBtn.isDisplayed) {
        XCTAssertNotNil(acceptBtn);
        XCTAssertNotNil([driver findElementBy:[SEBy name:@"利用規約"]]);
        XCTAssertNil([driver findElementBy:[SEBy name:@"同意しない"]]);
    } else {
        NSLog(@"Not in Term&Condition. SKIP!");
    }
}

- (void)test1_TermAndConditionActionTapAcceptButton {
    currentRunningMethod = NSStringFromSelector(_cmd);

//    [self exportToExcelFile];
    
    SEWebElement *acceptBtn = [driver findElementBy:[SEBy name:@"同意する"]];
    if (acceptBtn.isDisplayed) {
        [acceptBtn click];
    } else {
        NSLog(@"Not in Term&Condition. SKIP!");
    }
}

- (void)test2_WelcomeScreenLayout {
    currentRunningMethod = NSStringFromSelector(_cmd);
    
    XCTAssertNotNil([driver findElementBy:[SEBy name:@"新しくはじめる"]]);
    XCTAssertNotNil([driver findElementBy:[SEBy name:@"既にアカウントをお持ちの方"]]);
    XCTAssertNotNil([driver findElementBy:[SEBy xPath:@"//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]"]]);
    
    SEWebElement *welcomeTextEl = [driver findElementBy:[SEBy xPath:@"//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]"]];
    NSString *expectedStr = @"コミュニケーションを向上させる 企業向け画期的なコレボレーティブ・ツール。 社内・社外の枠を超えて、いつでも、どこからでも。";
//    NSLog(@"METHOD NAME: %@", NSStringFromSelector(_cmd));
    
    XCTAssertEqual([expectedStr isEqualToString:welcomeTextEl.text], YES);
}

- (void)test3_LoginLayout {
    currentRunningMethod = NSStringFromSelector(_cmd);
    
    SEWebElement *alreadyHaveAccBtn = [driver findElementBy:[SEBy name:@"既にアカウントをお持ちの方"]];
    [driver tapElement:alreadyHaveAccBtn];
    
    SEWebElement *loginBtn = [driver findElementBy:[SEBy name:@"ログイン"]];
    XCTAssertFalse(loginBtn.isEnabled);
    
    //ユーザーID
    SEWebElement *usernameTf = [driver findElementBy:[SEBy xPath:@"//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIATextField[1]"]];
    XCTAssertTrue(usernameTf.isDisplayed);
//    XCTAssertTrue([usernameTf attribute:@""])
    [usernameTf sendKeys:@"hungvt@mobilus.co.jp"];
    
    //パスワード
    SEWebElement *passwordTf = [driver findElementBy:[SEBy xPath:@"//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIASecureTextField[1]"]];
    XCTAssertTrue(passwordTf.isDisplayed);
    [passwordTf sendKeys:@"Mobilus123"];
    
    XCTAssertTrue(loginBtn.isEnabled);
    [loginBtn click];
}

- (void)test4_openLeftMenu {
    //The chat view is a CollectionView
    SEWebElement *collectionView = [driver findElementBy:[SEBy className:@"UIACollectionView"]];
    CGSize collectionSize = collectionView.size;
    [driver swipeFromStartX:0 startY:collectionSize.height/2 toEndX:collectionSize.width endY:collectionSize.height/2 duration:0.5];
    
}

/*
- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}
*/
@end
