//
//  SeleniumTests.m
//  SeleniumTests
//
//  Created by Dan Cuellar on 3/13/13.
//  Copyright (c) 2013 Appium. All rights reserved.
//

#import "SeleniumTests.h"
#import "SERemoteWebDriver.h"
#import "SECapabilities.h"
#import "SEBy.h"
#import "SEWebElement.h"

SERemoteWebDriver *driver;

@implementation SeleniumTests

- (void) setUp
{
    [super setUp];
    
    SECapabilities *caps = [SECapabilities new];
    [caps addCapabilityForKey:@"appium-version" andValue:@"1.0"];
    [caps setPlatformName:@"iOS"];
    [caps setPlatformVersion:@"8.1.2"];
    [caps setDeviceName:@"iPhone 5"];
    [caps setApp:@"/Users/vuongthanhhung/Desktop/[MobiWork][i-funbox.com].ipa"];
    NSError *error;
    driver = [[SERemoteWebDriver alloc] initWithServerAddress:@"127.0.0.1" port:4723 desiredCapabilities:caps requiredCapabilities:nil error:&error];

    [driver executeScript:@"UIALogger.logStart(\"Test Start\")"];
    [driver executeScript:@"UIALogger.logPass(\"Test Success\")"];
}

- (void) tearDown
{
    [driver quit];
    [super tearDown];
}

- (void) testUrl
{
    
	[driver setUrl:[[NSURL alloc] initWithString:@"http://www.zoosk.com"]];
	NSString *oldUrl = [[driver url] absoluteString];
	[driver setUrl:[[NSURL alloc] initWithString:@"http://appium.io/selenium-objective-c/index.html"]];
	XCTAssertFalse(([[[driver url] absoluteString] isEqualToString:oldUrl]), @"Url");
}

-(void) testSendKeys
{
	[driver setUrl:[[NSURL alloc] initWithString:@"http://appium.io/selenium-objective-c/testpages/textbox.html"]];
	SEWebElement *a = [driver findElementBy:[SEBy idString:@"text1"]];
	NSString *textToType = @"Hello World";
	[a sendKeys:textToType];
	NSString *typedText = [a text];
	XCTAssertTrue([typedText isEqualToString:textToType], @"SendKeys");
}

@end
